import os
import sys
from bs4 import BeautifulSoup
from lxml import etree, objectify
import requests
import re
from random import choice

section = r'.*<h1 class="css-ajupon e1qhuuqg0">Synonyms(.*)MOST RELEVANT<\/div><\/div><\/div><\/div>.*'
request = r'class="css-4b6kz6 etbu2a30">(\w*)<\/a>'
resultsDict = {}


def getSyns(word):
    if resultsDict.get(word, None) != None:
        return resultsDict[word]
    url = "http://www.thesaurus.com/browse/" + word
    print(url)
    page = requests.get(url)
    text = page.content.decode('utf-8')
    subtext = re.findall(section, text)
    if len(subtext) == 0:
        return []
    subtext = subtext[0]
    results = re.findall(request, subtext)
    res = []
    for r in results:
        res.append(r)
    resultsDict[word] = res
    print(res)
    return res

if __name__ == "__main__":
    string = "Check this out josiah, it is really cool."
    while True:
        outString = ""
        for word in re.findall(r"(\W*)(\w*)(\W*)", string):
            seq = getSyns(word[1])
            w = word[1]
            if len(seq) > 0 and word[1] != "I":
                w = choice(seq)
                if w.isupper() and len(word) > 2:
                    w = w.upper()
                elif word[1][0].isupper():
                    w = w[0].upper() + w[1:]
            outString += word[0] + w + word[2]
        print("Writing improved. Originally : {}".format(string))
        print("Final Output:")
        print(outString)
        newStr = input()
        if newStr != "":
            string = newStr
    # print(getSyns("Hello"))
